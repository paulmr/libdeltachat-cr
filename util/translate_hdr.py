# this is a simple script to parse the libdelta header file and create
# the crystal FFI definition.
#
# it's only neccessary to run this if the FFI def is out of date.
#
# it uses https://pyclibrary.readthedocs.io/ to parse the c header
#
# the c header was grabbed from:
#    https://github.com/deltachat/deltachat-core-rust/blob/main/deltachat-ffi/deltachat.h

from pyclibrary import CParser
from caseconverter import pascalcase
import re

hdr = CParser(['deltachat.h'])

print("""@[Link("deltachat")]\nlib LibDeltaChat""")

typemappings = {
        "char" : "LibC::Char",
        "int" : "LibC::Int",
        "double" : "LibC::Double",
        "size_t" : "LibC::SizeT",
        "void" : "Void",
        "dc_array_t" : "DCArray", # name clash
        }

voidtypes = set(["DCArray"])

def typemap(t):
    if t in typemappings:
        return typemappings[t]
    m = re.match(r'(u)?int([0-9]+)_t', t)
    if m != None:
        return (m.group(1).upper() if m.group(1) != None else "") + "Int" + m.group(2)
    m = re.match(r'dc_(.+)_t', t)
    if m != None:
        res = pascalcase(m.group(1))
        voidtypes.add(res)
        return res
    raise Exception("can't map type " + t)

def parse_function_type(name, ftype):
    rtype = typemap(ftype[0][0]) + (ftype[0][1] if len(ftype[0]) > 1 else "")
    args = [ t[0] + " : " + typemap(t[1][0]) + (t[1][1] if len(t[1]) > 1 else "") for t in ftype[1] ]
    return "fun " + name + "(" + ", ".join(args) + ") : " + rtype

constants = [ k[3:] + " = " + str(hdr.defs['values'][k]) for k in hdr.defs['values'] if k.startswith("DC_") ]
funcs = [ parse_function_type(k, v) for k,v in hdr.defs['functions'].items() ]
opaques = [ "type " + v + " = Void" for v in voidtypes ]

for section in [ opaques, constants, funcs ]:
    print("\n".join(section))

print("end")

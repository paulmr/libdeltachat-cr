module DeltaChat
  class Message
    getter id, text, from_id, chat_id
    def initialize(msg : DC::Msg*)
      @id = DC.dc_msg_get_id(msg)
      @text = DeltaChat.dc_string(DC.dc_msg_get_text(msg))
      @from_id = DC.dc_msg_get_from_id(msg)
      @chat_id = DC.dc_msg_get_chat_id(msg)
    end

    def from_self?
      from_id == DC::CONTACT_ID_SELF
    end
  end
end

module DeltaChat
  class Event
    getter id : EventId, data1_int, data2_int, data2_str
    def initialize(ev : DC::Event*)
      @id = EventId.new(DC.dc_event_get_id(ev))
      @data1_int = DC.dc_event_get_data1_int(ev)
      @data2_str = DC.dc_event_get_data2_str(ev) ? String.new(DC.dc_event_get_data2_str(ev)) : nil
      @data2_int = DC.dc_event_get_data2_int(ev)
    end
  end

  enum EventId
    # Tells that the Background fetch was completed (or timed out).
    AccountsBackgroundFetchDone = 2200
    # Chat ephemeral timer changed.
    ChatEphemeralTimerModified = 2021
    # Chat changed.
    ChatModified = 2020
    # A multi-device synced config value changed.
    ConfigSynced   = 2111
    # Inform about the configuration progress started by dc_configure().
    ConfigureProgress   = 2041
    # The connectivity to the server changed.
    ConnectivityChanged   = 2100
    # Contact(s) created, renamed, verified, blocked or deleted.
    ContactsChanged   = 2030
    # Emitted when a blob file was successfully deleted.
    DeletedBlobFile = 151
    # The library-user should report an error to the end-user.
    Error = 400
    # An action cannot be performed because the user is not in the group.
    ErrorSelfNotInGroup   = 410
    # Emitted when IMAP connection is established and login was successful.
    ImapConnected   = 102
    # Emitted before going into IDLE on the Inbox folder.
    ImapInboxIdle   = 106
    # Emitted when a message was successfully marked as deleted on the IMAP server.
    ImapMessageDeleted   = 104
    # Emitted when a message was successfully moved on IMAP.
    ImapMessageMoved   = 105
    # A file has been exported.
    ImexFileWritten   = 2052
    # Inform about the import/export progress started by dc_imex().
    ImexProgress   = 2051
    # There is a fresh message.
    IncomingMsg   = 2005
    # Downloading a bunch of messages just finished.
    IncomingMsgBunch   = 2006
    # The library-user may write an informational string to the log.
    Info = 100
    # Location of one or more contact has changed.
    LocationChanged   = 2035
    # A single message is deleted.
    MsgDeleted   = 2016
    # A single message is sent successfully.
    MsgDelivered   = 2010
    # A single message could not be sent.
    MsgFailed   = 2012
    # A single message is read by the receiver.
    MsgRead   = 2015
    # Messages or chats changed.
    MsgsChanged   = 2000
    # Messages were marked noticed or seen.
    MsgsNoticed   = 2008
    # Emitted when a new blob file was successfully written.
    NewBlobFile   = 150
    # Message reactions changed.
    ReactionsChanged   = 2001
    # Progress information of a secure-join handshake from the view of
    # the inviter (Alice, the person who shows the QR code).
    SecurejoinInviterProgress   = 2060
    # Progress information of a secure-join handshake from the view of
    # the joiner (Bob, the person who scans the QR code).
    SecurejoinJoinerProgress   = 2061
    # The user's avatar changed.
    SelfavatarChanged   = 2110
    # Emitted when SMTP connection is established and login was successful.
    SmtpConnected   = 101
    # Emitted when a message was successfully sent to the SMTP server.
    SmtpMessageSent   = 103
    # The library-user should write a warning string to the log.
    Warning   = 300
    # Message deleted which contained a webxdc instance.
    WebxdcInstanceDeleted   = 2121
    # webxdc status update received.
    WebxdcStatusUpdate   = 2120
  end
end

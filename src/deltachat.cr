module DeltaChat
  alias DC = LibDeltaChat
  # wrapper around the C context to make it easier to deal
  # with from crystal
  class Context
    def initialize(dbfile : String)
      @log = Log.for("deltachat")
      log.info { "DB file : #{dbfile}" }
      # os_name and blobdir are deprecated, always pass nil
      @ctx = DC.dc_context_new(nil, dbfile, nil)
    end

    private def log
      @log
    end

    def is_open? : Bool
      DC.dc_context_is_open(@ctx) == 1
    end

    def set_config(key, value)
      DC.dc_set_config(@ctx, key, value)
    end

    def set_config(entries)
      entries.each { |k,v| set_config(k, v) }
    end

    def get_last_error : String
      String.new(DC.dc_get_last_error(@ctx))
    end

    def config_from_qr(qr : String)
      if DC.dc_set_config_from_qr(@ctx, qr) != 1
        raise get_last_error
      end
    end

    def configured?
      DC.dc_is_configured(@ctx) == 1
    end

    def configure
      DC.dc_configure(@ctx)
    end

    def each_event(&)
      emitter = DC.dc_get_event_emitter(@ctx)
      while !(event = DC.dc_get_next_event(emitter)).nil?
        yield Event.new(event)
        DC.dc_event_unref(event)
        event = nil
      end
    ensure
      DC.dc_event_unref(event) if event
      DC.dc_event_emitter_unref(emitter) if emitter
    end

    def get_secure_join_qr(chat_id = 0)
      String.new(DC.dc_get_securejoin_qr(@ctx, chat_id))
    end

    def start_io
      DC.dc_start_io(@ctx)
    end

    def get_config(key : String) : String
      res = DC.dc_get_config(@ctx, key)
      str = String.new(res)
      DC.dc_str_unref(res)
      str
    end

    def get_message(id : UInt32)
      msg = DC.dc_get_msg(@ctx, id)
      Message.new(msg)
    ensure
      DC.dc_msg_unref(msg) if msg
    end

    def send_text_message(chat_id : UInt32, text : String)
      DC.dc_send_text_msg(@ctx, chat_id, text)
    end

    def get_contact(id)
      c = DC.dc_get_contact(@ctx, id)
      Contact.new(c)
    ensure
      DC.dc_contact_unref(c) if c
    end

    def get_contacts(flags : UInt32 = 0, query : String? = nil) : Array(Contact)
      res = [] of Contact
      arr = DC.dc_get_contacts(@ctx, flags, query)
      sz  = DC.dc_array_get_cnt(arr)
      DeltaChat.dc_array_each_id(arr) { |contact_id|
        res << get_contact(contact_id)
      }
      res
    end

    def markseen_msgs(msg_ids : Array(UInt32))
      DC.dc_markseen_msgs(@ctx, msg_ids.to_unsafe, msg_ids.size)
    end

    def reply_to_message(msg : Message, response : String)
      send_text_message(msg.chat_id, response)
    end

    def get_chat_msgs(chat_id : UInt32, flags : UInt32 = 0) : Array(Message)
      res = [] of Message
      arr = DC.dc_get_chat_msgs(@ctx, chat_id, flags, 0)
      DeltaChat.dc_array_each_id(arr) { |msg_id| res << get_message(msg_id) }
      res
    end

    def delete_chat(chat_id : UInt32)
      DC.dc_delete_chat(@ctx, chat_id)
    end

    def finalize
      DC.dc_context_unref(@ctx)
    end
  end
end

@[Link("deltachat")]
lib LibDeltaChat
  type Reactions = Void
  type Contact = Void
  type Accounts = Void
  type Chatlist = Void
  type DCArray = Void
  type BackupProvider = Void
  type Provider = Void
  type JsonrpcInstance = Void
  type EventEmitter = Void
  type Msg = Void
  type Lot = Void
  type Chat = Void
  type Event = Void
  type Context = Void
  CONNECTIVITY_NOT_CONNECTED            = 1000
  CONNECTIVITY_CONNECTING               = 2000
  CONNECTIVITY_WORKING                  = 3000
  CONNECTIVITY_CONNECTED                = 4000
  GCL_ARCHIVED_ONLY                     =    1
  GCL_NO_SPECIALS                       =    2
  GCL_ADD_ALLDONE_HINT                  =    4
  GCL_FOR_FORWARDING                    =    8
  GCM_ADDDAYMARKER                      =    1
  GCM_INFO_ONLY                         =    2
  GCL_VERIFIED_ONLY                     =    1
  GCL_ADD_SELF                          =    2
  IMEX_EXPORT_SELF_KEYS                 =    1
  IMEX_IMPORT_SELF_KEYS                 =    2
  IMEX_EXPORT_BACKUP                    =   11
  IMEX_IMPORT_BACKUP                    =   12
  QR_ASK_VERIFYCONTACT                  =  200
  QR_ASK_VERIFYGROUP                    =  202
  QR_FPR_OK                             =  210
  QR_FPR_MISMATCH                       =  220
  QR_FPR_WITHOUT_ADDR                   =  230
  QR_ACCOUNT                            =  250
  QR_BACKUP                             =  251
  QR_WEBRTC_INSTANCE                    =  260
  QR_ADDR                               =  320
  QR_TEXT                               =  330
  QR_URL                                =  332
  QR_ERROR                              =  400
  QR_WITHDRAW_VERIFYCONTACT             =  500
  QR_WITHDRAW_VERIFYGROUP               =  502
  QR_REVIVE_VERIFYCONTACT               =  510
  QR_REVIVE_VERIFYGROUP                 =  512
  QR_LOGIN                              =  520
  CHAT_ID_TRASH                         =    3
  CHAT_ID_ARCHIVED_LINK                 =    6
  CHAT_ID_ALLDONE_HINT                  =    7
  CHAT_ID_LAST_SPECIAL                  =    9
  MSG_ID_MARKER1                        =    1
  MSG_ID_DAYMARKER                      =    9
  MSG_ID_LAST_SPECIAL                   =    9
  INFO_UNKNOWN                          =    0
  INFO_GROUP_NAME_CHANGED               =    2
  INFO_GROUP_IMAGE_CHANGED              =    3
  INFO_MEMBER_ADDED_TO_GROUP            =    4
  INFO_MEMBER_REMOVED_FROM_GROUP        =    5
  INFO_AUTOCRYPT_SETUP_MESSAGE          =    6
  INFO_SECURE_JOIN_MESSAGE              =    7
  INFO_LOCATIONSTREAMING_ENABLED        =    8
  INFO_LOCATION_ONLY                    =    9
  INFO_EPHEMERAL_TIMER_CHANGED          =   10
  INFO_PROTECTION_ENABLED               =   11
  INFO_PROTECTION_DISABLED              =   12
  INFO_INVALID_UNENCRYPTED_MAIL         =   13
  INFO_WEBXDC_INFO_MESSAGE              =   32
  VIDEOCHATTYPE_UNKNOWN                 =    0
  VIDEOCHATTYPE_BASICWEBRTC             =    1
  VIDEOCHATTYPE_JITSI                   =    2
  CONTACT_ID_SELF                       =    1
  CONTACT_ID_INFO                       =    2
  CONTACT_ID_DEVICE                     =    5
  CONTACT_ID_LAST_SPECIAL               =    9
  TEXT1_DRAFT                           =    1
  TEXT1_USERNAME                        =    2
  TEXT1_SELF                            =    3
  MSG_TEXT                              =   10
  MSG_IMAGE                             =   20
  MSG_GIF                               =   21
  MSG_STICKER                           =   23
  MSG_AUDIO                             =   40
  MSG_VOICE                             =   41
  MSG_VIDEO                             =   50
  MSG_FILE                              =   60
  MSG_VIDEOCHAT_INVITATION              =   70
  MSG_WEBXDC                            =   80
  STATE_UNDEFINED                       =    0
  STATE_IN_FRESH                        =   10
  STATE_IN_NOTICED                      =   13
  STATE_IN_SEEN                         =   16
  STATE_OUT_PREPARING                   =   18
  STATE_OUT_DRAFT                       =   19
  STATE_OUT_PENDING                     =   20
  STATE_OUT_FAILED                      =   24
  STATE_OUT_DELIVERED                   =   26
  STATE_OUT_MDN_RCVD                    =   28
  CHAT_TYPE_UNDEFINED                   =    0
  CHAT_TYPE_SINGLE                      =  100
  CHAT_TYPE_GROUP                       =  120
  CHAT_TYPE_MAILINGLIST                 =  140
  CHAT_TYPE_BROADCAST                   =  160
  SOCKET_AUTO                           =    0
  SOCKET_SSL                            =    1
  SOCKET_STARTTLS                       =    2
  SOCKET_PLAIN                          =    3
  LP_AUTH_OAUTH2                        =    2
  LP_AUTH_NORMAL                        =    4
  LP_AUTH_FLAGS                         =    6
  CERTCK_AUTO                           =    0
  CERTCK_STRICT                         =    1
  CERTCK_ACCEPT_INVALID_CERTIFICATES    =    3
  EVENT_INFO                            =  100
  EVENT_SMTP_CONNECTED                  =  101
  EVENT_IMAP_CONNECTED                  =  102
  EVENT_SMTP_MESSAGE_SENT               =  103
  EVENT_IMAP_MESSAGE_DELETED            =  104
  EVENT_IMAP_MESSAGE_MOVED              =  105
  EVENT_IMAP_INBOX_IDLE                 =  106
  EVENT_NEW_BLOB_FILE                   =  150
  EVENT_DELETED_BLOB_FILE               =  151
  EVENT_WARNING                         =  300
  EVENT_ERROR                           =  400
  EVENT_ERROR_SELF_NOT_IN_GROUP         =  410
  EVENT_MSGS_CHANGED                    = 2000
  EVENT_REACTIONS_CHANGED               = 2001
  EVENT_INCOMING_MSG                    = 2005
  EVENT_INCOMING_MSG_BUNCH              = 2006
  EVENT_MSGS_NOTICED                    = 2008
  EVENT_MSG_DELIVERED                   = 2010
  EVENT_MSG_FAILED                      = 2012
  EVENT_MSG_READ                        = 2015
  EVENT_MSG_DELETED                     = 2016
  EVENT_CHAT_MODIFIED                   = 2020
  EVENT_CHAT_EPHEMERAL_TIMER_MODIFIED   = 2021
  EVENT_CONTACTS_CHANGED                = 2030
  EVENT_LOCATION_CHANGED                = 2035
  EVENT_CONFIGURE_PROGRESS              = 2041
  EVENT_IMEX_PROGRESS                   = 2051
  EVENT_IMEX_FILE_WRITTEN               = 2052
  EVENT_SECUREJOIN_INVITER_PROGRESS     = 2060
  EVENT_SECUREJOIN_JOINER_PROGRESS      = 2061
  EVENT_CONNECTIVITY_CHANGED            = 2100
  EVENT_SELFAVATAR_CHANGED              = 2110
  EVENT_CONFIG_SYNCED                   = 2111
  EVENT_WEBXDC_STATUS_UPDATE            = 2120
  EVENT_WEBXDC_INSTANCE_DELETED         = 2121
  EVENT_ACCOUNTS_BACKGROUND_FETCH_DONE  = 2200
  SHOW_EMAILS_OFF                       =    0
  SHOW_EMAILS_ACCEPTED_CONTACTS         =    1
  SHOW_EMAILS_ALL                       =    2
  MEDIA_QUALITY_BALANCED                =    0
  MEDIA_QUALITY_WORSE                   =    1
  KEY_GEN_DEFAULT                       =    0
  KEY_GEN_RSA2048                       =    1
  KEY_GEN_ED25519                       =    2
  KEY_GEN_RSA4096                       =    3
  PROVIDER_STATUS_OK                    =    1
  PROVIDER_STATUS_PREPARATION           =    2
  PROVIDER_STATUS_BROKEN                =    3
  CHAT_VISIBILITY_NORMAL                =    0
  CHAT_VISIBILITY_ARCHIVED              =    1
  CHAT_VISIBILITY_PINNED                =    2
  DOWNLOAD_DONE                         =    0
  DOWNLOAD_AVAILABLE                    =   10
  DOWNLOAD_FAILURE                      =   20
  DOWNLOAD_UNDECIPHERABLE               =   30
  DOWNLOAD_IN_PROGRESS                  = 1000
  STR_NOMESSAGES                        =    1
  STR_SELF                              =    2
  STR_DRAFT                             =    3
  STR_VOICEMESSAGE                      =    7
  STR_IMAGE                             =    9
  STR_VIDEO                             =   10
  STR_AUDIO                             =   11
  STR_FILE                              =   12
  STR_MSGGRPNAME                        =   15
  STR_MSGGRPIMGCHANGED                  =   16
  STR_MSGADDMEMBER                      =   17
  STR_MSGDELMEMBER                      =   18
  STR_MSGGROUPLEFT                      =   19
  STR_GIF                               =   23
  STR_ENCRYPTEDMSG                      =   24
  STR_E2E_AVAILABLE                     =   25
  STR_ENCR_TRANSP                       =   27
  STR_ENCR_NONE                         =   28
  STR_CANTDECRYPT_MSG_BODY              =   29
  STR_FINGERPRINTS                      =   30
  STR_READRCPT                          =   31
  STR_READRCPT_MAILBODY                 =   32
  STR_MSGGRPIMGDELETED                  =   33
  STR_E2E_PREFERRED                     =   34
  STR_CONTACT_VERIFIED                  =   35
  STR_CONTACT_NOT_VERIFIED              =   36
  STR_CONTACT_SETUP_CHANGED             =   37
  STR_ARCHIVEDCHATS                     =   40
  STR_AC_SETUP_MSG_SUBJECT              =   42
  STR_AC_SETUP_MSG_BODY                 =   43
  STR_CANNOT_LOGIN                      =   60
  STR_MSGACTIONBYUSER                   =   62
  STR_MSGACTIONBYME                     =   63
  STR_MSGLOCATIONENABLED                =   64
  STR_MSGLOCATIONDISABLED               =   65
  STR_LOCATION                          =   66
  STR_STICKER                           =   67
  STR_DEVICE_MESSAGES                   =   68
  STR_SAVED_MESSAGES                    =   69
  STR_DEVICE_MESSAGES_HINT              =   70
  STR_WELCOME_MESSAGE                   =   71
  STR_UNKNOWN_SENDER_FOR_CHAT           =   72
  STR_SUBJECT_FOR_NEW_CONTACT           =   73
  STR_FAILED_SENDING_TO                 =   74
  STR_EPHEMERAL_DISABLED                =   75
  STR_EPHEMERAL_SECONDS                 =   76
  STR_EPHEMERAL_MINUTE                  =   77
  STR_EPHEMERAL_HOUR                    =   78
  STR_EPHEMERAL_DAY                     =   79
  STR_EPHEMERAL_WEEK                    =   80
  STR_EPHEMERAL_FOUR_WEEKS              =   81
  STR_VIDEOCHAT_INVITATION              =   82
  STR_VIDEOCHAT_INVITE_MSG_BODY         =   83
  STR_CONFIGURATION_FAILED              =   84
  STR_BAD_TIME_MSG_BODY                 =   85
  STR_UPDATE_REMINDER_MSG_BODY          =   86
  STR_ERROR_NO_NETWORK                  =   87
  STR_REPLY_NOUN                        =   90
  STR_SELF_DELETED_MSG_BODY             =   91
  STR_SERVER_TURNED_OFF                 =   92
  STR_EPHEMERAL_MINUTES                 =   93
  STR_EPHEMERAL_HOURS                   =   94
  STR_EPHEMERAL_DAYS                    =   95
  STR_EPHEMERAL_WEEKS                   =   96
  STR_FORWARDED                         =   97
  STR_QUOTA_EXCEEDING_MSG_BODY          =   98
  STR_PARTIAL_DOWNLOAD_MSG_BODY         =   99
  STR_DOWNLOAD_AVAILABILITY             =  100
  STR_SYNC_MSG_SUBJECT                  =  101
  STR_SYNC_MSG_BODY                     =  102
  STR_INCOMING_MESSAGES                 =  103
  STR_OUTGOING_MESSAGES                 =  104
  STR_STORAGE_ON_DOMAIN                 =  105
  STR_ONE_MOMENT                        =  106
  STR_CONNECTED                         =  107
  STR_CONNTECTING                       =  108
  STR_UPDATING                          =  109
  STR_SENDING                           =  110
  STR_LAST_MSG_SENT_SUCCESSFULLY        =  111
  STR_ERROR                             =  112
  STR_NOT_SUPPORTED_BY_PROVIDER         =  113
  STR_MESSAGES                          =  114
  STR_BROADCAST_LIST                    =  115
  STR_PART_OF_TOTAL_USED                =  116
  STR_SECURE_JOIN_STARTED               =  117
  STR_SECURE_JOIN_REPLIES               =  118
  STR_SETUP_CONTACT_QR_DESC             =  119
  STR_SECURE_JOIN_GROUP_QR_DESC         =  120
  STR_NOT_CONNECTED                     =  121
  STR_AEAP_ADDR_CHANGED                 =  122
  STR_AEAP_EXPLANATION_AND_LINK         =  123
  STR_GROUP_NAME_CHANGED_BY_YOU         =  124
  STR_GROUP_NAME_CHANGED_BY_OTHER       =  125
  STR_GROUP_IMAGE_CHANGED_BY_YOU        =  126
  STR_GROUP_IMAGE_CHANGED_BY_OTHER      =  127
  STR_ADD_MEMBER_BY_YOU                 =  128
  STR_ADD_MEMBER_BY_OTHER               =  129
  STR_REMOVE_MEMBER_BY_YOU              =  130
  STR_REMOVE_MEMBER_BY_OTHER            =  131
  STR_GROUP_LEFT_BY_YOU                 =  132
  STR_GROUP_LEFT_BY_OTHER               =  133
  STR_GROUP_IMAGE_DELETED_BY_YOU        =  134
  STR_GROUP_IMAGE_DELETED_BY_OTHER      =  135
  STR_LOCATION_ENABLED_BY_YOU           =  136
  STR_LOCATION_ENABLED_BY_OTHER         =  137
  STR_EPHEMERAL_TIMER_DISABLED_BY_YOU   =  138
  STR_EPHEMERAL_TIMER_DISABLED_BY_OTHER =  139
  STR_EPHEMERAL_TIMER_SECONDS_BY_YOU    =  140
  STR_EPHEMERAL_TIMER_SECONDS_BY_OTHER  =  141
  STR_EPHEMERAL_TIMER_1_MINUTE_BY_YOU   =  142
  STR_EPHEMERAL_TIMER_1_MINUTE_BY_OTHER =  143
  STR_EPHEMERAL_TIMER_1_HOUR_BY_YOU     =  144
  STR_EPHEMERAL_TIMER_1_HOUR_BY_OTHER   =  145
  STR_EPHEMERAL_TIMER_1_DAY_BY_YOU      =  146
  STR_EPHEMERAL_TIMER_1_DAY_BY_OTHER    =  147
  STR_EPHEMERAL_TIMER_1_WEEK_BY_YOU     =  148
  STR_EPHEMERAL_TIMER_1_WEEK_BY_OTHER   =  149
  STR_EPHEMERAL_TIMER_MINUTES_BY_YOU    =  150
  STR_EPHEMERAL_TIMER_MINUTES_BY_OTHER  =  151
  STR_EPHEMERAL_TIMER_HOURS_BY_YOU      =  152
  STR_EPHEMERAL_TIMER_HOURS_BY_OTHER    =  153
  STR_EPHEMERAL_TIMER_DAYS_BY_YOU       =  154
  STR_EPHEMERAL_TIMER_DAYS_BY_OTHER     =  155
  STR_EPHEMERAL_TIMER_WEEKS_BY_YOU      =  156
  STR_EPHEMERAL_TIMER_WEEKS_BY_OTHER    =  157
  STR_BACKUP_TRANSFER_QR                =  162
  STR_BACKUP_TRANSFER_MSG_BODY          =  163
  STR_CHAT_PROTECTION_ENABLED           =  170
  STR_CHAT_PROTECTION_DISABLED          =  171
  STR_NEW_GROUP_SEND_FIRST_MESSAGE      =  172
  STR_MESSAGE_ADD_MEMBER                =  173
  STR_INVALID_UNENCRYPTED_MAIL          =  174
  fun dc_context_new(os_name : LibC::Char*, dbfile : LibC::Char*, blobdir : LibC::Char*) : Context*
  fun dc_context_new_closed(dbfile : LibC::Char*) : Context*
  fun dc_context_open(context : Context*, passphrase : LibC::Char*) : LibC::Int
  fun dc_context_change_passphrase(context : Context*, passphrase : LibC::Char*) : LibC::Int
  fun dc_context_is_open(context : Context*) : LibC::Int
  fun dc_context_unref(context : Context*) : Void
  fun dc_get_id(context : Context*) : UInt32
  fun dc_get_event_emitter(context : Context*) : EventEmitter*
  fun dc_get_blobdir(context : Context*) : LibC::Char*
  fun dc_set_config(context : Context*, key : LibC::Char*, value : LibC::Char*) : LibC::Int
  fun dc_get_config(context : Context*, key : LibC::Char*) : LibC::Char*
  fun dc_set_stock_translation(context : Context*, stock_id : UInt32, stock_msg : LibC::Char*) : LibC::Int
  fun dc_set_config_from_qr(context : Context*, qr : LibC::Char*) : LibC::Int
  fun dc_get_info(context : Context*) : LibC::Char*
  fun dc_get_oauth2_url(context : Context*, addr : LibC::Char*, redirect_uri : LibC::Char*) : LibC::Char*
  fun dc_get_connectivity(context : Context*) : LibC::Int
  fun dc_get_connectivity_html(context : Context*) : LibC::Char*
  fun dc_all_work_done(context : Context*) : LibC::Int
  fun dc_configure(context : Context*) : Void
  fun dc_is_configured(context : Context*) : LibC::Int
  fun dc_start_io(context : Context*) : Void
  fun dc_stop_io(context : Context*) : Void
  fun dc_maybe_network(context : Context*) : Void
  fun dc_preconfigure_keypair(context : Context*, addr : LibC::Char*, public_data : LibC::Char*, secret_data : LibC::Char*) : LibC::Int
  fun dc_get_chatlist(context : Context*, flags : LibC::Int, query_str : LibC::Char*, query_id : UInt32) : Chatlist*
  fun dc_create_chat_by_contact_id(context : Context*, contact_id : UInt32) : UInt32
  fun dc_get_chat_id_by_contact_id(context : Context*, contact_id : UInt32) : UInt32
  fun dc_prepare_msg(context : Context*, chat_id : UInt32, msg : Msg*) : UInt32
  fun dc_send_msg(context : Context*, chat_id : UInt32, msg : Msg*) : UInt32
  fun dc_send_msg_sync(context : Context*, chat_id : UInt32, msg : Msg*) : UInt32
  fun dc_send_text_msg(context : Context*, chat_id : UInt32, text_to_send : LibC::Char*) : UInt32
  fun dc_send_videochat_invitation(context : Context*, chat_id : UInt32) : UInt32
  fun dc_send_reaction(context : Context*, msg_id : UInt32, reaction : LibC::Char*) : UInt32
  fun dc_get_msg_reactions(context : Context*, msg_id : LibC::Int) : Reactions*
  fun dc_send_webxdc_status_update(context : Context*, msg_id : UInt32, json : LibC::Char*, descr : LibC::Char*) : LibC::Int
  fun dc_get_webxdc_status_updates(context : Context*, msg_id : UInt32, serial : UInt32) : LibC::Char*
  fun dc_set_draft(context : Context*, chat_id : UInt32, msg : Msg*) : Void
  fun dc_add_device_msg(context : Context*, label : LibC::Char*, msg : Msg*) : UInt32
  fun dc_was_device_msg_ever_added(context : Context*, label : LibC::Char*) : LibC::Int
  fun dc_get_draft(context : Context*, chat_id : UInt32) : Msg*
  fun dc_get_chat_msgs(context : Context*, chat_id : UInt32, flags : UInt32, marker1before : UInt32) : DCArray*
  fun dc_get_msg_cnt(context : Context*, chat_id : UInt32) : LibC::Int
  fun dc_get_fresh_msg_cnt(context : Context*, chat_id : UInt32) : LibC::Int
  fun dc_get_similar_chatlist(context : Context*, chat_id : UInt32) : Chatlist*
  fun dc_estimate_deletion_cnt(context : Context*, from_server : LibC::Int, seconds : Int64) : LibC::Int
  fun dc_get_fresh_msgs(context : Context*) : DCArray*
  fun dc_get_next_msgs(context : Context*) : DCArray*
  fun dc_wait_next_msgs(context : Context*) : DCArray*
  fun dc_marknoticed_chat(context : Context*, chat_id : UInt32) : Void
  fun dc_get_chat_media(context : Context*, chat_id : UInt32, msg_type : LibC::Int, msg_type2 : LibC::Int, msg_type3 : LibC::Int) : DCArray*
  fun dc_get_next_media(context : Context*, msg_id : UInt32, dir : LibC::Int, msg_type : LibC::Int, msg_type2 : LibC::Int, msg_type3 : LibC::Int) : UInt32
  fun dc_set_chat_visibility(context : Context*, chat_id : UInt32, visibility : LibC::Int) : Void
  fun dc_delete_chat(context : Context*, chat_id : UInt32) : Void
  fun dc_block_chat(context : Context*, chat_id : UInt32) : Void
  fun dc_accept_chat(context : Context*, chat_id : UInt32) : Void
  fun dc_get_chat_contacts(context : Context*, chat_id : UInt32) : DCArray*
  fun dc_get_chat_encrinfo(context : Context*, chat_id : UInt32) : LibC::Char*
  fun dc_get_chat_ephemeral_timer(context : Context*, chat_id : UInt32) : UInt32
  fun dc_search_msgs(context : Context*, chat_id : UInt32, query : LibC::Char*) : DCArray*
  fun dc_get_chat(context : Context*, chat_id : UInt32) : Chat*
  fun dc_create_group_chat(context : Context*, protect : LibC::Int, name : LibC::Char*) : UInt32
  fun dc_create_broadcast_list(context : Context*) : UInt32
  fun dc_is_contact_in_chat(context : Context*, chat_id : UInt32, contact_id : UInt32) : LibC::Int
  fun dc_add_contact_to_chat(context : Context*, chat_id : UInt32, contact_id : UInt32) : LibC::Int
  fun dc_remove_contact_from_chat(context : Context*, chat_id : UInt32, contact_id : UInt32) : LibC::Int
  fun dc_set_chat_name(context : Context*, chat_id : UInt32, name : LibC::Char*) : LibC::Int
  fun dc_set_chat_ephemeral_timer(context : Context*, chat_id : UInt32, timer : UInt32) : LibC::Int
  fun dc_set_chat_profile_image(context : Context*, chat_id : UInt32, image : LibC::Char*) : LibC::Int
  fun dc_set_chat_mute_duration(context : Context*, chat_id : UInt32, duration : Int64) : LibC::Int
  fun dc_get_msg_info(context : Context*, msg_id : UInt32) : LibC::Char*
  fun dc_get_msg_html(context : Context*, msg_id : UInt32) : LibC::Char*
  fun dc_download_full_msg(context : Context*, msg_id : LibC::Int) : Void
  fun dc_get_mime_headers(context : Context*, msg_id : UInt32) : LibC::Char*
  fun dc_delete_msgs(context : Context*, msg_ids : UInt32*, msg_cnt : LibC::Int) : Void
  fun dc_forward_msgs(context : Context*, msg_ids : UInt32*, msg_cnt : LibC::Int, chat_id : UInt32) : Void
  fun dc_resend_msgs(context : Context*, msg_ids : UInt32*, msg_cnt : LibC::Int) : LibC::Int
  fun dc_markseen_msgs(context : Context*, msg_ids : UInt32*, msg_cnt : LibC::Int) : Void
  fun dc_get_msg(context : Context*, msg_id : UInt32) : Msg*
  fun dc_may_be_valid_addr(addr : LibC::Char*) : LibC::Int
  fun dc_lookup_contact_id_by_addr(context : Context*, addr : LibC::Char*) : UInt32
  fun dc_create_contact(context : Context*, name : LibC::Char*, addr : LibC::Char*) : UInt32
  fun dc_add_address_book(context : Context*, addr_book : LibC::Char*) : LibC::Int
  fun dc_get_contacts(context : Context*, flags : UInt32, query : LibC::Char*) : DCArray*
  fun dc_get_blocked_cnt(context : Context*) : LibC::Int
  fun dc_get_blocked_contacts(context : Context*) : DCArray*
  fun dc_block_contact(context : Context*, contact_id : UInt32, block : LibC::Int) : Void
  fun dc_get_contact_encrinfo(context : Context*, contact_id : UInt32) : LibC::Char*
  fun dc_delete_contact(context : Context*, contact_id : UInt32) : LibC::Int
  fun dc_get_contact(context : Context*, contact_id : UInt32) : Contact*
  fun dc_imex(context : Context*, what : LibC::Int, param1 : LibC::Char*, param2 : LibC::Char*) : Void
  fun dc_imex_has_backup(context : Context*, dir : LibC::Char*) : LibC::Char*
  fun dc_initiate_key_transfer(context : Context*) : LibC::Char*
  fun dc_continue_key_transfer(context : Context*, msg_id : UInt32, setup_code : LibC::Char*) : LibC::Int
  fun dc_stop_ongoing_process(context : Context*) : Void
  fun dc_check_qr(context : Context*, qr : LibC::Char*) : Lot*
  fun dc_get_securejoin_qr(context : Context*, chat_id : UInt32) : LibC::Char*
  fun dc_get_securejoin_qr_svg(context : Context*, chat_id : UInt32) : LibC::Char*
  fun dc_join_securejoin(context : Context*, qr : LibC::Char*) : UInt32
  fun dc_send_locations_to_chat(context : Context*, chat_id : UInt32, seconds : LibC::Int) : Void
  fun dc_is_sending_locations_to_chat(context : Context*, chat_id : UInt32) : LibC::Int
  fun dc_set_location(context : Context*, latitude : LibC::Double, longitude : LibC::Double, accuracy : LibC::Double) : LibC::Int
  fun dc_get_locations(context : Context*, chat_id : UInt32, contact_id : UInt32, timestamp_begin : Int64, timestamp_end : Int64) : DCArray*
  fun dc_delete_all_locations(context : Context*) : Void
  fun dc_get_last_error(context : Context*) : LibC::Char*
  fun dc_str_unref(str : LibC::Char*) : Void
  fun dc_backup_provider_new(context : Context*) : BackupProvider*
  fun dc_backup_provider_get_qr(backup_provider : BackupProvider*) : LibC::Char*
  fun dc_backup_provider_get_qr_svg(backup_provider : BackupProvider*) : LibC::Char*
  fun dc_backup_provider_wait(backup_provider : BackupProvider*) : Void
  fun dc_backup_provider_unref(backup_provider : BackupProvider*) : Void
  fun dc_receive_backup(context : Context*, qr : LibC::Char*) : LibC::Int
  fun dc_accounts_new(dir : LibC::Char*, writable : LibC::Int) : Accounts*
  fun dc_accounts_unref(accounts : Accounts*) : Void
  fun dc_accounts_add_account(accounts : Accounts*) : UInt32
  fun dc_accounts_add_closed_account(accounts : Accounts*) : UInt32
  fun dc_accounts_migrate_account(accounts : Accounts*, dbfile : LibC::Char*) : UInt32
  fun dc_accounts_remove_account(accounts : Accounts*, account_id : UInt32) : LibC::Int
  fun dc_accounts_get_all(accounts : Accounts*) : DCArray*
  fun dc_accounts_get_account(accounts : Accounts*, account_id : UInt32) : Context*
  fun dc_accounts_get_selected_account(accounts : Accounts*) : Context*
  fun dc_accounts_select_account(accounts : Accounts*, account_id : UInt32) : LibC::Int
  fun dc_accounts_all_work_done(accounts : Accounts*) : LibC::Int
  fun dc_accounts_start_io(accounts : Accounts*) : Void
  fun dc_accounts_stop_io(accounts : Accounts*) : Void
  fun dc_accounts_maybe_network(accounts : Accounts*) : Void
  fun dc_accounts_maybe_network_lost(accounts : Accounts*) : Void
  fun dc_accounts_background_fetch(accounts : Accounts*, timeout : UInt64) : LibC::Int
  fun dc_accounts_get_event_emitter(accounts : Accounts*) : EventEmitter*
  fun dc_array_unref(array : DCArray*) : Void
  fun dc_array_get_cnt(array : DCArray*) : LibC::SizeT
  fun dc_array_get_id(array : DCArray*, index : LibC::SizeT) : UInt32
  fun dc_array_get_latitude(array : DCArray*, index : LibC::SizeT) : LibC::Double
  fun dc_array_get_longitude(array : DCArray*, index : LibC::SizeT) : LibC::Double
  fun dc_array_get_accuracy(array : DCArray*, index : LibC::SizeT) : LibC::Double
  fun dc_array_get_timestamp(array : DCArray*, index : LibC::SizeT) : Int64
  fun dc_array_get_chat_id(array : DCArray*, index : LibC::SizeT) : UInt32
  fun dc_array_get_contact_id(array : DCArray*, index : LibC::SizeT) : UInt32
  fun dc_array_get_msg_id(array : DCArray*, index : LibC::SizeT) : UInt32
  fun dc_array_get_marker(array : DCArray*, index : LibC::SizeT) : LibC::Char*
  fun dc_array_is_independent(array : DCArray*, index : LibC::SizeT) : LibC::Int
  fun dc_array_search_id(array : DCArray*, needle : UInt32, ret_index : LibC::SizeT*) : LibC::Int
  fun dc_chatlist_unref(chatlist : Chatlist*) : Void
  fun dc_chatlist_get_cnt(chatlist : Chatlist*) : LibC::SizeT
  fun dc_chatlist_get_chat_id(chatlist : Chatlist*, index : LibC::SizeT) : UInt32
  fun dc_chatlist_get_msg_id(chatlist : Chatlist*, index : LibC::SizeT) : UInt32
  fun dc_chatlist_get_summary(chatlist : Chatlist*, index : LibC::SizeT, chat : Chat*) : Lot*
  fun dc_chatlist_get_summary2(context : Context*, chat_id : UInt32, msg_id : UInt32) : Lot*
  fun dc_chatlist_get_context(chatlist : Chatlist*) : Context*
  fun dc_chat_get_info_json(context : Context*, chat_id : LibC::SizeT) : LibC::Char*
  fun dc_chat_unref(chat : Chat*) : Void
  fun dc_chat_get_id(chat : Chat*) : UInt32
  fun dc_chat_get_type(chat : Chat*) : LibC::Int
  fun dc_chat_get_mailinglist_addr(chat : Chat*) : LibC::Char*
  fun dc_chat_get_name(chat : Chat*) : LibC::Char*
  fun dc_chat_get_profile_image(chat : Chat*) : LibC::Char*
  fun dc_chat_get_color(chat : Chat*) : UInt32
  fun dc_chat_get_visibility(chat : Chat*) : LibC::Int
  fun dc_chat_is_contact_request(chat : Chat*) : LibC::Int
  fun dc_chat_is_unpromoted(chat : Chat*) : LibC::Int
  fun dc_chat_is_self_talk(chat : Chat*) : LibC::Int
  fun dc_chat_is_device_talk(chat : Chat*) : LibC::Int
  fun dc_chat_can_send(chat : Chat*) : LibC::Int
  fun dc_chat_is_protected(chat : Chat*) : LibC::Int
  fun dc_chat_is_protection_broken(chat : Chat*) : LibC::Int
  fun dc_chat_is_sending_locations(chat : Chat*) : LibC::Int
  fun dc_chat_is_muted(chat : Chat*) : LibC::Int
  fun dc_chat_get_remaining_mute_duration(chat : Chat*) : Int64
  fun dc_msg_new(context : Context*, viewtype : LibC::Int) : Msg*
  fun dc_msg_unref(msg : Msg*) : Void
  fun dc_msg_get_id(msg : Msg*) : UInt32
  fun dc_msg_get_from_id(msg : Msg*) : UInt32
  fun dc_msg_get_chat_id(msg : Msg*) : UInt32
  fun dc_msg_get_viewtype(msg : Msg*) : LibC::Int
  fun dc_msg_get_state(msg : Msg*) : LibC::Int
  fun dc_msg_get_timestamp(msg : Msg*) : Int64
  fun dc_msg_get_received_timestamp(msg : Msg*) : Int64
  fun dc_msg_get_sort_timestamp(msg : Msg*) : Int64
  fun dc_msg_get_text(msg : Msg*) : LibC::Char*
  fun dc_msg_get_subject(msg : Msg*) : LibC::Char*
  fun dc_msg_get_file(msg : Msg*) : LibC::Char*
  fun dc_msg_get_filename(msg : Msg*) : LibC::Char*
  fun dc_msg_get_filemime(msg : Msg*) : LibC::Char*
  fun dc_msg_get_webxdc_blob(msg : Msg*, filename : LibC::Char*, ret_bytes : LibC::SizeT*) : LibC::Char*
  fun dc_msg_get_webxdc_info(msg : Msg*) : LibC::Char*
  fun dc_msg_get_filebytes(msg : Msg*) : UInt64
  fun dc_msg_get_width(msg : Msg*) : LibC::Int
  fun dc_msg_get_height(msg : Msg*) : LibC::Int
  fun dc_msg_get_duration(msg : Msg*) : LibC::Int
  fun dc_msg_get_showpadlock(msg : Msg*) : LibC::Int
  fun dc_msg_is_bot(msg : Msg*) : LibC::Int
  fun dc_msg_get_ephemeral_timer(msg : Msg*) : UInt32
  fun dc_msg_get_ephemeral_timestamp(msg : Msg*) : Int64
  fun dc_msg_get_summary(msg : Msg*, chat : Chat*) : Lot*
  fun dc_msg_get_summarytext(msg : Msg*, approx_characters : LibC::Int) : LibC::Char*
  fun dc_msg_get_override_sender_name(msg : Msg*) : LibC::Char*
  fun dc_msg_has_deviating_timestamp(msg : Msg*) : LibC::Int
  fun dc_msg_has_location(msg : Msg*) : LibC::Int
  fun dc_msg_is_sent(msg : Msg*) : LibC::Int
  fun dc_msg_is_forwarded(msg : Msg*) : LibC::Int
  fun dc_msg_is_info(msg : Msg*) : LibC::Int
  fun dc_msg_get_info_type(msg : Msg*) : LibC::Int
  fun dc_msg_is_increation(msg : Msg*) : LibC::Int
  fun dc_msg_is_setupmessage(msg : Msg*) : LibC::Int
  fun dc_msg_get_setupcodebegin(msg : Msg*) : LibC::Char*
  fun dc_msg_get_videochat_url(msg : Msg*) : LibC::Char*
  fun dc_msg_get_error(msg : Msg*) : LibC::Char*
  fun dc_msg_get_videochat_type(msg : Msg*) : LibC::Int
  fun dc_msg_has_html(msg : Msg*) : LibC::Int
  fun dc_msg_get_download_state(msg : Msg*) : LibC::Int
  fun dc_msg_set_text(msg : Msg*, text : LibC::Char*) : Void
  fun dc_msg_set_html(msg : Msg*, html : LibC::Char*) : Void
  fun dc_msg_set_subject(msg : Msg*, subject : LibC::Char*) : Void
  fun dc_msg_set_override_sender_name(msg : Msg*, name : LibC::Char*) : Void
  fun dc_msg_set_file(msg : Msg*, file : LibC::Char*, filemime : LibC::Char*) : Void
  fun dc_msg_set_dimension(msg : Msg*, width : LibC::Int, height : LibC::Int) : Void
  fun dc_msg_set_duration(msg : Msg*, duration : LibC::Int) : Void
  fun dc_msg_set_location(msg : Msg*, latitude : LibC::Double, longitude : LibC::Double) : Void
  fun dc_msg_latefiling_mediasize(msg : Msg*, width : LibC::Int, height : LibC::Int, duration : LibC::Int) : Void
  fun dc_msg_set_quote(msg : Msg*, quote : Msg*) : Void
  fun dc_msg_get_quoted_text(msg : Msg*) : LibC::Char*
  fun dc_msg_get_quoted_msg(msg : Msg*) : Msg*
  fun dc_msg_get_parent(msg : Msg*) : Msg*
  fun dc_msg_force_plaintext(msg : Msg*) : Void
  fun dc_contact_unref(contact : Contact*) : Void
  fun dc_contact_get_id(contact : Contact*) : UInt32
  fun dc_contact_get_addr(contact : Contact*) : LibC::Char*
  fun dc_contact_get_name(contact : Contact*) : LibC::Char*
  fun dc_contact_get_auth_name(contact : Contact*) : LibC::Char*
  fun dc_contact_get_display_name(contact : Contact*) : LibC::Char*
  fun dc_contact_get_name_n_addr(contact : Contact*) : LibC::Char*
  fun dc_contact_get_profile_image(contact : Contact*) : LibC::Char*
  fun dc_contact_get_color(contact : Contact*) : UInt32
  fun dc_contact_get_status(contact : Contact*) : LibC::Char*
  fun dc_contact_get_last_seen(contact : Contact*) : Int64
  fun dc_contact_was_seen_recently(contact : Contact*) : LibC::Int
  fun dc_contact_is_blocked(contact : Contact*) : LibC::Int
  fun dc_contact_is_verified(contact : Contact*) : LibC::Int
  fun dc_contact_is_bot(contact : Contact*) : LibC::Int
  fun dc_contact_get_verifier_id(contact : Contact*) : UInt32
  fun dc_provider_new_from_email(context : Context*, email : LibC::Char*) : Provider*
  fun dc_provider_new_from_email_with_dns(context : Context*, email : LibC::Char*) : Provider*
  fun dc_provider_get_overview_page(provider : Provider*) : LibC::Char*
  fun dc_provider_get_before_login_hint(provider : Provider*) : LibC::Char*
  fun dc_provider_get_status(provider : Provider*) : LibC::Int
  fun dc_provider_unref(provider : Provider*) : Void
  fun dc_lot_unref(lot : Lot*) : Void
  fun dc_lot_get_text1(lot : Lot*) : LibC::Char*
  fun dc_lot_get_text2(lot : Lot*) : LibC::Char*
  fun dc_lot_get_text1_meaning(lot : Lot*) : LibC::Int
  fun dc_lot_get_state(lot : Lot*) : LibC::Int
  fun dc_lot_get_id(lot : Lot*) : UInt32
  fun dc_lot_get_timestamp(lot : Lot*) : Int64
  fun dc_reactions_get_contacts(reactions : Reactions*) : DCArray*
  fun dc_reactions_get_by_contact_id(reactions : Reactions*, contact_id : UInt32) : LibC::Char*
  fun dc_reactions_unref(reactions : Reactions*) : Void
  fun dc_jsonrpc_init(account_manager : Accounts*) : JsonrpcInstance*
  fun dc_jsonrpc_unref(jsonrpc_instance : JsonrpcInstance*) : Void
  fun dc_jsonrpc_request(jsonrpc_instance : JsonrpcInstance*, request : LibC::Char*) : Void
  fun dc_jsonrpc_next_response(jsonrpc_instance : JsonrpcInstance*) : LibC::Char*
  fun dc_jsonrpc_blocking_call(jsonrpc_instance : JsonrpcInstance*, input : LibC::Char*) : LibC::Char*
  fun dc_get_next_event(emitter : EventEmitter*) : Event*
  fun dc_event_emitter_unref(emitter : EventEmitter*) : Void
  fun dc_event_get_id(event : Event*) : LibC::Int
  fun dc_event_get_data1_int(event : Event*) : LibC::Int
  fun dc_event_get_data2_int(event : Event*) : LibC::Int
  fun dc_event_get_data2_str(event : Event*) : LibC::Char*
  fun dc_event_get_account_id(event : Event*) : UInt32
  fun dc_event_unref(event : Event*) : Void
end

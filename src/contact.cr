module DeltaChat
  class Contact
    getter id, addr, verified
    def initialize(c : DC::Contact*)
      @addr = DeltaChat.dc_string(DC.dc_contact_get_addr(c))
      @id = DC.dc_contact_get_id(c)
      @verified = DC.dc_contact_is_verified(c)
    end
  end
end

module DeltaChat
  def self.dc_string(s : LibC::Char*) : String
    res = String.new(s)
    DC.dc_str_unref(s)
    res
  end

  # taks an array, calls block for each id, and then frees it
  def self.dc_array_each_id(arr : DC::DCArray*, &)
    sz = DC.dc_array_get_cnt(arr)
    (0...sz).each do |idx|
      yield DC.dc_array_get_id(arr, idx)
    end
  ensure
    DC.dc_array_unref(arr) if arr
  end
end
